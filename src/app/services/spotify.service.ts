import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) { }

  getQuery( query: string ){

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBYQHSlbfweS86lEaZyADtYubayvGZcG9sZrpYgCX9Ic1WH7cTMTY32ijp32f2mqrzqRKsT6JpVxwbIpgY'
    });

    return this.http.get(url, { headers });
  }

  getNewReleases(){

    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQCYKNMl-LXg9BtA84c-vF_4bmkF5JDNAUbaYjUihahzyyi-aRytDEuOwCf1BVLxyPi2E5qfqTZxKg-ScBY'
    // });

    return this.getQuery('browse/new-releases')
      .pipe( map( data => data['albums'].items));

  }

  getArtists( termino: string ){

    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQCYKNMl-LXg9BtA84c-vF_4bmkF5JDNAUbaYjUihahzyyi-aRytDEuOwCf1BVLxyPi2E5qfqTZxKg-ScBY  '
    // });

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( data => data['artists'].items));

  }

  getArtist( id: string ){

    return this.getQuery(`artists/${ id }`);
      // .pipe( map( data => data['artists'].items));
  }

  getToTraks( id: string){
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
       .pipe( map( data => data['tracks']));
  }
}
