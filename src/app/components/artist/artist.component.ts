import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router'
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  artist: any = {};
  topTracks: any[] = [];
  loading: boolean;

  constructor( private activatedRoute: ActivatedRoute,
               private spotifyService: SpotifyService) { 

    this.activatedRoute.params.subscribe( data => {

      this.getArtist( data['id']);
      this.getTopTracks( data['id']);

    });
  }

  ngOnInit(): void {
  }

  getArtist( id: string){

    this.loading = true;

    this.spotifyService.getArtist( id )
      .subscribe( data => {
        console.log(data);
        this.artist = data;
        this.loading = false;
      });
  }

  getTopTracks( id: string){

    this.loading = true;

    this.spotifyService.getToTraks( id )
      .subscribe( data => {
        console.log(data);
        this.topTracks = data;
      });
  }

}
