import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  newSongs: any[] = [];
  loading: boolean;
  error: boolean;
  mensajeError: string;

  constructor( private spotify: SpotifyService) {

    this.loading = true;
    this.error = false;

    this.spotify.getNewReleases()
      .subscribe( ( data: any ) => {
        console.log( data );
        this.newSongs = data;
        this.loading = false;
      }, ( errorServicio ) => {
        this.loading = false;
        this.error = true;
       this.mensajeError = errorServicio.error.error.message;
        
      });
  }

  ngOnInit(): void {
  }

}
